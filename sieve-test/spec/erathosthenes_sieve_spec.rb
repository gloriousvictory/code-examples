require_relative '../lib/eratosthenes_sieve'

RSpec.describe EratosthenesSieve do
  describe '#generate' do
    before :each do
      @e = EratosthenesSieve.new
    end

    context 'max_prime is 10' do
      it 'returns 2,3,5,7' do
        expect(@e.generate(10)).to eql [2,3,5,7] 
      end
    end

    context 'max_prime is 2' do
      it 'raises error' do
        expect(@e.generate(2)).to eql [2]
      end
    end

    context 'max_prime is nil' do
      it 'raises error' do
        expect { @e.generate(nil) }.to raise_error(RuntimeError)
      end
    end

    context 'max_prime is 0' do
      it 'raises error' do
        expect { @e.generate(0) }.to raise_error(RuntimeError)
      end
    end

    context 'max_prime is 1' do
      it 'raises error' do
        expect { @e.generate(1) }.to raise_error(RuntimeError)
      end
    end
  end
end
