require_relative '../lib/console_printer'

RSpec.describe ConsolePrinter do
  before :each do
    @cp = ConsolePrinter.new
  end

  describe '#generate' do
    context 'arguments not arrays' do
      it 'raise error' do
        expect { @cp.generate(1,1) }.to raise_error(RuntimeError)
      end
    end

    it 'calls print_header' do
      expect(@cp).to receive(:print_header).with([2])
      @cp.generate([2],[4])
    end

    it 'calls print_body' do
      expect(@cp).to receive(:print_body).with([2],[4])
      @cp.generate([2],[4])
    end
  end
end
