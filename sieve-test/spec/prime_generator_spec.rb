require_relative '../lib/prime_generator'

RSpec.describe PrimeGenerator do
  describe '#generate' do
    context 'called directly' do
      it 'raises error' do
        pg = PrimeGenerator.new
        expect { pg.generate(1) }.to raise_error(RuntimeError)
      end
    end
  end
end
