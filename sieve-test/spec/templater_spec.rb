require_relative '../lib/templater.rb'

RSpec.describe Templater do
  describe '#generate' do
    context 'called directly' do
      it 'raises error' do
        t = Templater.new
        expect { t.generate(1,1) }.to raise_error(RuntimeError)
      end
    end 
  end
end
