require_relative 'templater'

class ConsolePrinter < Templater
  def generate(x, y)
    raise 'Arguments must be array' unless x.kind_of?(Array) && y.kind_of?(Array)
    print_header(x)
    print_body(x, y)  
  end

  def print_header(x)
    print '     '
    x.each {|i| print "%-3d  " % i}
    print "\n"
    print '---- '
    x.each {|i| print '---- '}
    print "\n"
  end

  def print_body(x, y)
    y.each do |j|
      print "%-3d| " % j
      x.each {|i| print "%-3d  " % (i*j)}
      print "\n"
    end
  end
end
