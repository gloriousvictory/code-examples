require_relative 'prime_multiples_generator'
require_relative 'eratosthenes_sieve'
require_relative 'console_printer'

prime_generator = PrimeMultiplesGenerator.new
prime_generator.prime_generator = EratosthenesSieve.new
prime_generator.templater = ConsolePrinter.new

prime_numbers = prime_generator.print(10)
