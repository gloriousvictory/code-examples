class PrimeMultiplesGenerator
  attr_accessor :prime_generator, :templater

  def generate_primes(max_prime)
    @prime_generator.generate(max_prime)
  end

  def print(max_prime)
    primes = generate_primes(max_prime)
    @templater.generate(primes, primes)
  end
end
