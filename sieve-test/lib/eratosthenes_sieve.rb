require_relative 'prime_generator'

class EratosthenesSieve < PrimeGenerator
  def generate(max_prime)
    # Create a list of consecutive integers from 2 through n: (2, 3, 4, ..., n).
    # Initially, let p equal 2, the first prime number.
    # Starting from p, enumerate its multiples by counting to n in increments of p, and mark them in the list (these will be 2p, 3p, 4p, ... ; the p itself should not be marked).
    # Find the first number greater than p in the list that is not marked. If there was no such number, stop. Otherwise, let p now equal this new number (which is the next prime), and repeat from step 3.

    raise 'max_prime must not be nil' if max_prime.eql?(nil)
    raise 'max_prime must be at least 2' if max_prime < 2

    primes = []
    not_primes = []

    consecutive_integers = (2..max_prime) # create a list of consecutive integers

    consecutive_integers.each do |current_int|
      next if not_primes.include? current_int # skip if we know this isn't a prime number

      (current_int..max_prime).step(current_int) do |multiple_val| # enumerate multiples
        not_primes << multiple_val unless current_int.eql?(multiple_val) # add to not_primes apart from the first time around the loop
      end

      primes << current_int unless not_primes.include?(current_int) # if its not in the not_primes, it must be a prime...
    end

    primes
  end
end
