class Search
  def chop(search_for, array_of_ints)
    raise 'INTERFACE CLASS'
  end
end

class RecursiveSearch < Search
  def chop (search_for, array_of_ints)
    sorted_array = array_of_ints.sort!
    return search(sorted_array, search_for, 0, sorted_array.length-1)
  end

  private 
  def search(values, target, start_val, end_val)
    return -1 if start_val > end_val

    middle_element = (start_val + end_val / 2).floor
    value = values[middle_element]

    return search(values, target, start_val, middle_element-1) if value > target
    return search(values, target, middle_element+1, end_val) if value < target

    return middle_element
  end
end

class IterativeSearch < Search
  def chop(search_for, array_of_ints)
    sorted_array = array_of_ints.sort!

    first = 0
    last = sorted_array.length-1
    index = -1
    found = false

    while (first < last) && found.eql?(false) do
      middle_element = (first + last / 2).floor
      value = sorted_array[middle_element]

      if value == search_for
        index = middle_element
        found = true
      end

      if search_for < value 
        last = middle_element-1        
      else
        first = middle_element+1
      end
    end

    return index
  end
end

rs = RecursiveSearch.new
puts rs.chop(1, [1,3,5,7])

is = IterativeSearch.new
puts is.chop(5, [1,3,5,7])
