file = File.open('weather.dat', 'rb')

data = file.each_line.map(&:split)

data.map! do |line| # map! overwrites each element
  line.take(3).map!(&:to_i) # only take the first 3 elements, cast to int
end

data.delete_if { |line| line.empty? || line[0].eql?(0) }

top_spread = nil
top_element = nil

top_spread = data[0][1] - data[0][2]
top_element = data[0][0]

data.each do |line|
  spread = line[1] - line[2]

  if spread < top_spread
    top_element = line[0]
    top_spread = spread
  end
end

puts "Top Element: #{top_element}"
puts "Top Spread: #{top_spread}"
