require_relative "../bogof_rule"
require_relative "../item"

RSpec.describe BogofRule do
  describe '#get_discount' do
    context 'items contains no GR1' do
      it 'should return 0' do
        b = BogofRule.new
        item = Item.new('SR1', 'Strawberries', 5)
        items = [item]

        b.get_discount(items).should eq(0)
      end
    end

    context 'items contains two GR1' do
      it 'should return 3.11' do
        b = BogofRule.new
        item = Item.new('GR1', 'Green Tea', 3.11)
        items = [item, item]

        b.get_discount(items).should eq(3.11)        
      end
    end

    context 'items contains three GR1' do
      it 'should return 3.11' do
        b = BogofRule.new
        item = Item.new('GR1', 'Green Tea', 3.11)
        items = [item, item, item]

        b.get_discount(items).should eq(3.11)        
      end
    end
  end

  describe '#item_price' do
  end
end
