require_relative 'pricing_rule'

class FourFiddyRule < PricingRule
  def get_discount(items)
    item_count = get_item_count(items, "SR1")

    if item_count >= 3
      return item_count*item_discount_price
    else
      return 0
    end
  end

  def item_discount_price
    0.5
  end
end
