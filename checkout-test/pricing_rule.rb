class PricingRule
  def get_discount(items)
    raise! 'PricingRule#get_discount not defined'
  end

  protected
  def get_item_count(items, item_code)
    item_count = items.select{|item| item.code.eql?(item_code)}.count    
  end
end
