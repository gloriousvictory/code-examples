require_relative 'checkout'
require_relative 'item'
require_relative 'bogof_rule'
require_relative 'four_fiddy_rule'

# The rules
bogof = BogofRule.new
four_fiddy = FourFiddyRule.new

# Drop them into an array
pricing_rules = [bogof, four_fiddy]

@checkout = Checkout.new(pricing_rules)

# These could probably be wrapped up into their own classes
# e.g
# strawberries = Strawberries.new
# green_tea = GreenTea.new

strawberries = Item.new('SR1', 'Strawberries', 5)
green_tea = Item.new('GR1', 'Green Tea', 3.11)
coffee = Item.new('CF1', 'Coffee', 11.23)

# Test 1
# Basket: GR1,SR1,GR1,GR1,CF1
# Total price expected: £22.45 
@checkout.scan(green_tea)
@checkout.scan(strawberries)
@checkout.scan(green_tea)
@checkout.scan(green_tea)
@checkout.scan(coffee)

puts "Basket: GR1,SR1,GR1,GR1,CF1"
puts "Total price expected: £22.45"
puts "Total price: #{@checkout.total}"
puts "\n"

@checkout.reset

# Test 2
# Basket: GR1,GR1
# Total price expected: £3.11
@checkout.scan(green_tea)
@checkout.scan(green_tea)

puts "Basket: GR1,GR1"
puts "Total price expected: £3.11"
puts "Total price: #{@checkout.total}"
puts "\n"

@checkout.reset


# Test 3
# Basket: SR1,SR1,GR1,SR1
# Total price expected: £16.61
@checkout.scan(strawberries)
@checkout.scan(strawberries)
@checkout.scan(green_tea)
@checkout.scan(strawberries)

puts "Basket: SR1,SR1,GR1,SR1"
puts "Total price expected: £16.61"
puts "Total price: #{@checkout.total}"
