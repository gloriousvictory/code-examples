class Checkout
  @@scanned_items = []
  @@pricing_rules = []
  @@total = 0 # current balance of the checkout

  def initialize(pricing_rules)
    @@pricing_rules = pricing_rules
  end

  def scan(item)
    @@scanned_items << item
    @@total += item.price
  end

  def reset
    @@scanned_items = []
    @@total = 0
  end

  def total
    # No discount to start with...
    discount = 0

    # Apply discount based on pricing rules    
    @@pricing_rules.each do |rule|
      discount += rule.get_discount(@@scanned_items)
    end

    return @@total - discount 
  end
end
