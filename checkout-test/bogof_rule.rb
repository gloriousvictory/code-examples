require_relative 'pricing_rule'

class BogofRule < PricingRule
  def get_discount(items)
    # get total number of suitable items
    item_count = get_item_count(items, 'GR1')

    # BYGOF means dividing by 2 to get relevant items
    # Ruby rounds down by default so no need to mess about with modulus
    number_of_bogof = item_count/2

    return number_of_bogof*item_discount_price 
  end

  def item_discount_price
    3.11
  end

end
